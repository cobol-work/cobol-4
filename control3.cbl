       IDENTIFICATION DIVISION.
       PROGRAM-ID.  CONTROL3.
       AUTHOR. VIRAPAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  CITY-CODE   PIC 9        VALUE ZERO .
           88 CITY-IS-DUBLIN        VALUE 1.
           88 CITY-IS-LIMERICK      VALUE 1.
           88 CITY-IS-CORK          VALUE 1.
           88 CITY-IS-GALWAY        VALUE 1.
           88 CITY-IS-SLIGO         VALUE 1.
           88 CITY-IS-WATERFORD     VALUE 1.
           88 UNIVERSITY-CITY       VALUE 1 THRU 4.
           88 CITY-CODE-NOT-VALID   VALUE 0 , 7 , 8 , 9.
       PROCEDURE DIVISION .
       BEGINPROGRAM.
           DISPLAY "Enter a city code (1-6) :" WITH NO ADVANCING 
           ACCEPT CITY-CODE 

           IF CITY-CODE-NOT-VALID  THEN
              DISPLAY "Invalid City code entered"
           ELSE
              IF CITY-IS-LIMERICK  THEN
                 DISPLAY "Hey , We're home"
              END-IF
              IF CITY-IS-DUBLIN  THEN
                 DISPLAY "Hey , We're in the capital"
              END-IF
              IF UNIVERSITY-CITY   THEN
                 DISPLAY "Apply the rent surchage ! "
              END-IF    
           END-IF
           SET CITY-IS-DUBLIN TO TRUE
           DISPLAY CITY-CODE  
           GOBACK  
           .   