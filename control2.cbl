       IDENTIFICATION DIVISION.
       PROGRAM-ID.  CONTROL2.
       AUTHOR. VIRAPAT.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           CLASS HEX-NUMBER IS "0" THRU "9" , "A" THRU "F"
           CLASS REAL-NAME  IS "A" THRU "Z" , "a" THRU "z" , "'" ,SPACE.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 NUM-IN   PIC X(4).
       01 NAME-IN  PIC X(15).

       PROCEDURE DIVISION.
       BEGINPROGRAM.
           DISPLAY "Enter a Hex number - " WITH NO ADVANCING
           ACCEPT NUM-IN 
           IF NUM-IN IS HEX-NUMBER THEN
              DISPLAY NUM-IN " is a hex number"
           ELSE
              DISPLAY NUM-IN " is not a hex number"
           END-IF

            DISPLAY "Enter a real name : " WITH NO ADVANCING
           ACCEPT NAME-IN 
           IF NAME-IN IS REAL-NAME THEN
              DISPLAY NAME-IN " is a real name"
           ELSE
              DISPLAY NAME-IN " is not a reak name"
           END-IF

           DISPLAY "Enter a name : " WITH NO ADVANCING 
           ACCEPT NAME-IN 
           IF NAME-IN IS ALPHABETIC  THEN
              DISPLAY NAME-IN " is a name"
           ELSE    
              DISPLAY NAME-IN " is a not name"
           END-IF
           .   
            